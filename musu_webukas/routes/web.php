<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::resource('products', 'ProductController')->only(['index']);
Route::resource('categories', 'CategoryController')->only(['show']);
Route::resource('cart', 'CartController')->only(['index', 'store', 'destroy']);
Route::post('/checkout', 'CartController@checkout')->name('cart.checkout');
Route::resource('contacts', 'ContactController')->only(['index', 'store']);

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('auth')->group(function () {
    Route::get('/', 'DashboardController')->name('dashboard');
    Route::resource('orders', 'OrderController')->only(['index', 'show', 'destroy']);
    Route::resource('products', 'ProductController');
    Route::resource('categories', 'CategoryController');
    Route::resource('contacts', 'ContactController')->only(['index', 'destroy']);
});
