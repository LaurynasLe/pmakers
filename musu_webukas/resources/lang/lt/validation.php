<?php

return [
  'required' => ':Attribute yra privalomas.',
  'numeric' => ':Attribute turi būti skaičius.',
  'exists' => 'Pasirinktas neegzistuojantis :attribute',
  'image' => ':Attribute turi būti paveiksliukas',
  'uploaded' => 'Nepavyko įkelti :attribute',

  'attributes' => [
    'title' => 'pavadinimas',
    'description' => 'aprašymas',
    'price' => 'kaina',
    'photo' => 'nuotrauka',
    'category_id' => 'kategorija',
  ],
];