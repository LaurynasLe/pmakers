@extends('layouts.app')

@section('title', $category->title)

@section('content')
@component('components.product-list', ['categories' => $categories, 'products' => $category->products]) @endcomponent
@endsection