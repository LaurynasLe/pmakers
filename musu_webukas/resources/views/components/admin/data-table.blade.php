<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        {{ $headings }}
      </tr>
    </thead>
    <tbody>
      {{ $slot }}
    </tbody>
  </table>
</div>