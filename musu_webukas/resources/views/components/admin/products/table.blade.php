@component('components.admin.data-table')
@slot('headings')
<th scope="col">#</th>
<th scope="col">Pavadinimas</th>
@endslot
@foreach ($products as $product)
<tr>
  <th scope="row">{{ $product->id }}</th>
  <td>
    <a href={{ route('admin.products.show', ['id' => $product->id]) }}>
      {{ $product->title }}
    </a>
  </td>
</tr>
@endforeach
@endcomponent