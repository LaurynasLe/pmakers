<div class="form-group">
  <label for="{{ $name }}">{{ $title }}</label>
  {{ $slot }}
</div>