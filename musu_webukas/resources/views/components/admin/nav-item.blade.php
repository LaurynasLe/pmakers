<li class="nav-item">
  <a class="nav-link @if (Route::is($route)) active @endif" href="{{ route($route) }}">
    <i data-feather="{{ $icon }}"></i>
    {{ $slot }}
    @if (Route::is($route))
    <span class="sr-only">(pasirinkta)</span>
    @endif
  </a>
</li>