<a class="nav-item nav-link @if (Request::is($url)) active @endif" href="{{ $url }}">
  {{ $slot }}
  @if (Request::is($url))
  <span class="sr-only">(pasirinkta)</span>
  @endif
</a>