<div class="container">
  <nav class="nav nav-pills nav-justified my-5">
    @component('components.nav-item', ['url' => route('products.index')])
    Visos paslaugos
    @endcomponent
    @foreach ($categories as $category)
    @component('components.nav-item', ['url' => route('categories.show', ['category' => $category])])
    {{ $category->title }}
    @endcomponent
    @endforeach
  </nav>

  <div class="row">
    @foreach ($products as $product)
    <div class="col-md-3">
      <div class="card mb-4 box-shadow">
        <img src="{{ Storage::url($product->photoPath) }}" alt="{{ $product->title }}" class="card-img-top">
        <div class="card-body">
          <h3 class="card-title">{{ $product->title }}</h3>
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="card-title">{{ $product->price }} &euro;</h5>
            <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
              data-target="#product-{{ $product->id }}-modal">
              Plačiau
            </button>
          </div>
        </div>
      </div>
    </div>
    <form action="{{ route('cart.store') }}" method="POST">
      @csrf
      <input type="hidden" name="product_id" value="{{ $product->id}}">
      <div class="modal fade" id="product-{{ $product->id }}-modal" tabindex="-1" role="dialog"
        aria-labelledby="product-{{ $product->id }}-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="product-{{ $product->id }}-modal-label">{{ $product->title }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="{{ Storage::url($product->photoPath) }}" alt="{{ $product->title }}" class="img-fluid">
              <p>{!! nl2br(e($product->description)) !!}</p>
              @if ($product->quantifiable)
              <div class="form-group">
                <label for="product-{{ $product->id }}-modal-quantity" class="col-form-label">Kiekis:</label>
                <input type="number" name="quantity" class="form-control" id="product-{{ $product->id }}-modal-quantity"
                  placeholder="1" value="{{ old('quantity', 1) }}" required>
                @error('quantity')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
              </div>
              @endif
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Atšaukti</button>
              <button type="submit" class="btn btn-primary"><i data-feather="shopping-cart"></i> {{ $product->price }}
                &euro;</button>
            </div>
          </div>
        </div>
      </div>
    </form>
    @endforeach
  </div>
</div>