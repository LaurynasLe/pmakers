@extends('layouts.app')

@section('title', 'Paslaugos')

@section('content')
@component('components.product-list', ['categories' => $categories, 'products' => $products]) @endcomponent
@endsection