@extends('layouts.app')

@section('title', 'Krepšelis')

@section('content')
<div class="container">

  <div class="row">
    <div class="col">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Paslauga</th>
            <th scope="col">Kaina</th>
            <th scope="col">Kiekis</th>
            <th scope="col">Suma &euro;</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($items as $item)
          <tr>
            <th scope="row">{{ $item['product']->title }}</th>
            <td>{{ $item['product']->price}}</td>
            <td>{{ $item['quantity'] }}</td>
            <td>{{ $item['price'] }}</td>
            <td>
              <form action="{{ route('cart.destroy', ['product_id' => $item['product']->id]) }}" method="POST">
                @csrf
                @method("DELETE")
                <button type="submit" class="btn btn-danger"><i data-feather="trash"></i></button>
              </form>
            </td>
          </tr>
          @endforeach
          <tr>
            <th scope="row">Suma</th>
            <td></td>
            <td></td>
            <td>{{ $price }}</td>
            <td></td>
          </tr>
        </tbody>
      </table>

      <hr class="mb-4">
      <a role="button" href="{{ route('products.index') }}" class="btn btn-link btn-lg btn-block">Pridėti daugiau
        paslaugų</a>
    </div>
    <div class="col">
      @if ($price > 0)
      <form action="{{ route('cart.checkout') }}" method="POST">
        @csrf
        @component('components.admin.form-group', ['name' => 'customer_name'])
        @slot('title') Vardas @endslot
        <input id="customer_name" name="customer_name" value="{{ old('customer_name') }}" type="text"
          class="form-control @error('customer_name') is-invalid @enderror" placeholder="Vardenis Pavardenis" required>
        @error('customer_name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        @endcomponent

        @component('components.admin.form-group', ['name' => 'customer_phone'])
        @slot('title') Telefono numeris @endslot
        <input id="customer_phone" name="customer_phone" value="{{ old('customer_phone') }}" type="text"
          class="form-control @error('customer_phone') is-invalid @enderror" placeholder="+370..." required>
        @error('customer_phone')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        @endcomponent

        @component('components.admin.form-group', ['name' => 'customer_email'])
        @slot('title') El. pašto adresas @endslot
        <input id="customer_email" name="customer_email" value="{{ old('customer_email') }}" type="email"
          class="form-control @error('customer_email') is-invalid @enderror" placeholder="@" required>
        @error('customer_email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        @endcomponent

        <hr class="mb-4">
        <button type="submit" class="btn btn-primary btn-lg btn-block">Užsakyti</button>
      </form>
      @endif
    </div>
  </div>
</div>
@endsection