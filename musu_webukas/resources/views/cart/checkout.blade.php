@extends('layouts.app')

@section('title', "Užsakymas $order->id sėkmingai pateiktas")

@section('content')
<div class="container">
  <a role="button" class="btn btn-primary btn-lg btn-block" href="{{ route('products.index') }}">Grįžti į produktų
    sąrašą</a>
</div>
@endsection