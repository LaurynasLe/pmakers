@extends('layouts.admin')

@section('title', 'Užklausos')

@section('content')
@component('components.admin.data-table')
@slot('headings')
<th scope="col">#</th>
<th scope="col">Vardas</th>
<th scope="col">Telefono Nr.</th>
<th scope="col">El. paštas</th>
<th scope="col">Užklausa</th>
<th scope="col"></th>
@endslot
@foreach ($contacts as $contact)
<tr>
  <th scope="row">{{ $contact->id }}</th>
  <td>{{ $contact->name }}</td>
  <td>{{ $contact->phone }}</td>
  <td>{{ $contact->email }}</td>
  <td>{{ $contact->text }}</td>
  <td>
    <form action="{{ route('admin.contacts.destroy', ['contact' => $contact]) }}" method="POST">
      @csrf
      @method("DELETE")
      <button type="submit" class="btn btn-danger"><i data-feather="trash"></i></button>
    </form>
  </td>
</tr>
@endforeach
@endcomponent
@endsection