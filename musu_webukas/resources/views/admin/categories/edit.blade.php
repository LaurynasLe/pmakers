@extends('layouts.admin')

@section('title', 'Keisti kategoriją')

@section('content')
<form action="{{ route('admin.categories.update', ['category' => $category]) }}" method="POST">
  @method('PUT')
  @csrf
  @component('components.admin.form-group', ['name' => 'title'])
  @slot('title') Pavadinimas @endslot
  <input id="title" name="title" value="{{ old('title', $category->title) }}" type="text"
    class="form-control @error('title') is-invalid @enderror" placeholder="Kategorijos pavadinimas" required>
  @error('title')
  <div class="invalid-feedback">
    {{ $message }}
  </div>
  @enderror
  @endcomponent
  <hr class="mb-4">
  <button type="submit" class="btn btn-primary btn-lg btn-block">Išsaugoti</button>
</form>
@endsection