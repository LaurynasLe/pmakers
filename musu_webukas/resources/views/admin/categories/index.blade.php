@extends('layouts.admin')

@section('title', 'Kategorijos')

@section('buttons')
<div class="btn-group mr-2">
  <a role="button" class="btn btn-sm btn-primary" href={{ route('admin.categories.create') }}>
    <i data-feather="plus-circle"></i>
    Nauja kategorija
  </a>
</div>
@endsection

@section('content')
@component('components.admin.data-table')
@slot('headings')
<th scope="col">#</th>
<th scope="col">Pavadinimas</th>
@endslot
@foreach ($categories as $category)
<tr>
  <th scope="row">{{ $category->id }}</th>
  <td>
    <a href={{ route('admin.categories.show', ['id' => $category->id]) }}>
      {{ $category->title }}
    </a>
  </td>
</tr>
@endforeach
@endcomponent
@endsection