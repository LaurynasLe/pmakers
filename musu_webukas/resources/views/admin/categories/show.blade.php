@extends('layouts.admin')

@section('title', $category->title)

@section('buttons')
<div class="btn-group mr-2">
  <a role="button" class="btn btn-sm btn-primary" href={{ route('admin.categories.edit', ['id' => $category->id]) }}>
    <i data-feather="edit"></i>
    Keisti
  </a>
  <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
    data-target="#delete-category-{{ $category->id }}">
    <i data-feather="trash"></i>
    Ištrinti
  </button>
</div>

@component('components.admin.modal', ['id' => "delete-category-$category->id"])
@slot('title') Ištrinti {{ $category->title }} @endslot
Ar tikrai norite ištrinti <code>{{ $category->title }}</code>?
@slot('buttons')
<button type="button" class="btn btn-primary" data-dismiss="modal">Atšaukti</button>
<button type="button" class="btn btn-danger"
  onclick="document.getElementById('delete-category-{{ $category->id }}-form').submit();">Ištrinti</button>
@endslot
@endcomponent
<form id="delete-category-{{ $category->id }}-form"
  action="{{ route('admin.categories.destroy', ['id' => $category->id]) }}" method="POST" style="display: none;">
  @csrf
  @method('DELETE')
</form>
@endsection

@section('content')
<h3>Paslaugos</h3>
@component('components.admin.products.table', ['products' => $category->products]) @endcomponent
@endsection