@extends('layouts.admin')

@section('title', 'Keisti paslaugą')

@section('content')
<form action="{{ route('admin.products.update', ['product' => $product]) }}" method="POST"
  enctype="multipart/form-data">
  @method('PUT')
  @csrf

  @component('components.admin.form-group', ['name' => 'category_id'])
  @slot('title') Kategorija @endslot
  <div class="input-group">
    <select class="custom-select @error('category') is-invalid @enderror" id="category_id" name="category_id" required>
      @foreach ($categories as $category)
      <option @if (old('category_id', $product->category_id)==$category->id) selected @endif
        value="{{ $category->id }}">
        {{ $category->title }}
      </option>
      @endforeach
    </select>
    <div class="input-group-append">
      <a role="button" class="btn btn-primary" href="{{ route('admin.categories.create') }}">Nauja kategorija</a>
    </div>
  </div>
  @error('category_id')
  <div class="invalid-feedback">
    {{ $message }}
  </div>
  @enderror
  @endcomponent

  @component('components.admin.form-group', ['name' => 'title'])
  @slot('title') Pavadinimas @endslot
  <input id="title" name="title" value="{{ old('title', $product->title) }}" type="text"
    class="form-control @error('title') is-invalid @enderror" placeholder="Paslaugos pavadinimas" required>
  @error('title')
  <div class="invalid-feedback">
    {{ $message }}
  </div>
  @enderror
  @endcomponent

  @component('components.admin.form-group', ['name' => 'description'])
  @slot('title') Aprašymas @endslot
  <textarea name="description" id="description" cols="30" rows="10"
    class="form-control @error('description') is-invalid @enderror" placeholder="Paslaugos aprašymas"
    required>{{ old('description', $product->description) }}</textarea>
  @error('description')
  <div class="invalid-feedback">
    {{ $message }}
  </div>
  @enderror
  @endcomponent

  <div class="form-row">
    <div class="col">
      <label for="price">Kaina</label>
      <div class="input-group">
        <input id="price" name="price" value="{{ old('price', $product->price) }}" type="number" step="0.01"
          class="form-control @error('price') is-invalid @enderror" placeholder="0.00" required>
        <div class="input-group-append">
          <span class="input-group-text">&euro;</span>
        </div>
      </div>
      @error('price')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="col">
      <label for="photo">Nauja nuotrauka</label>
      <input type="file" name="photo" class="form-control-file @error('photo') is-invalid @enderror" id="photo">
      @error('photo')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
  </div>
  <div class="form-group form-check">
    <input name="quantifiable" type="checkbox" id="quantifiable"
      class="form-check-input @error('price') is-invalid @enderror" @if (old('quantifiable', $product->quantifiable))
    checked @endif>
    <label class="form-check-label" for="quantifiable">
      Paslaugą galima užsisakyti su kiekiu
    </label>
    @error('quantifiable')
    <div class="invalid-feedback">
      {{ $message }}
    </div>
    @enderror
  </div>
  <hr class="mb-4">
  <button type="submit" class="btn btn-primary btn-lg btn-block">Išsaugoti</button>
</form>
@endsection