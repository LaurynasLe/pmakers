@extends('layouts.admin')

@section('title', 'Paslaugos')

@section('buttons')
<div class="btn-group mr-2">
  <a role="button" class="btn btn-sm btn-primary" href={{ route('admin.products.create') }}>
    <i data-feather="plus-circle"></i>
    Nauja paslauga
  </a>
</div>
@endsection

@section('content')
@component('components.admin.products.table', ['products' => $products]) @endcomponent
@endsection