@extends('layouts.admin')

@section('title', $product->title)

@section('buttons')
<div class="btn-group mr-2">
  <a role="button" class="btn btn-sm btn-primary" href={{ route('admin.products.edit', ['product' => $product]) }}>
    <i data-feather="edit"></i>
    Keisti
  </a>
  <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
    data-target="#delete-product-{{ $product->id }}">
    <i data-feather="trash"></i>
    Ištrinti
  </button>
</div>
@component('components.admin.modal', ['id' => "delete-product-$product->id", 'title' => "Ištrinti $product->title"])
@slot('title') Ištrinti {{ $product->title }} @endslot
Ar tikrai norite ištrinti <code>{{ $product->title }}</code>?
@slot('buttons')
<button type="button" class="btn btn-primary" data-dismiss="modal">Atšaukti</button>
<button type="button" class="btn btn-danger"
  onclick="document.getElementById('delete-product-{{ $product->id }}-form').submit();">Ištrinti</button>
@endslot
@endcomponent
<form id="delete-product-{{ $product->id }}-form"
  action="{{ route('admin.products.destroy', ['product' => $product]) }}" method="POST" style="display: none;">
  @csrf
  @method('DELETE')
</form>
@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a
        href="{{ route('admin.categories.show', ['category' => $product->category]) }}">{{ $product->category->title }}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">{{ $product->title }}</li>
  </ol>
</nav>
<img src="{{ Storage::url($product->photoPath) }}" alt="{{ $product->title }}" class="img-thumbnail"
  style="max-width: 512px">
<p>{{ $product->description }}</p>
<p>@if ($product->quantifiable) Šią paslaugą galima užsisakyti su kiekiu @else Galima užsisakyti tik vieną paslaugą
  @endif</p>
<p>Kaina {{ $product->price }} &euro;</p>
@endsection