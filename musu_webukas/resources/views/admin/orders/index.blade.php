@extends('layouts.admin')

@section('title', 'Užsakymai')

@section('content')
@component('components.admin.data-table')
@slot('headings')
<th scope="col">#</th>
<th scope="col">Data</th>
<th scope="col">Kaina</th>
@endslot
@foreach ($orders as $order)
<tr>
  <th scope="row">{{ $order->id }}</th>
  <td>
    <a href={{ route('admin.orders.show', ['order' => $order]) }}>
      {{ $order->created_at }}
    </a>
  </td>
  <td>
    {{ $order->price }}
  </td>
</tr>
@endforeach
@endcomponent
@endsection