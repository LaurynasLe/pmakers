@extends('layouts.admin')

@section('title', "Užsakymas $order->id: $order->created_at")

@section('buttons')
<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-order-{{ $order->id }}">
  <i data-feather="trash"></i>
  Ištrinti
</button>

@component('components.admin.modal', ['id' => "delete-order-$order->id", 'title' => "Ištrinti $order->title"])
@slot('title') Ištrinti {{ $order->title }} @endslot
Ar tikrai norite ištrinti <code>{{ $order->title }}</code>?
@slot('buttons')
<button type="button" class="btn btn-primary" data-dismiss="modal">Atšaukti</button>
<form id="delete-order-{{ $order->id }}-form" action="{{ route('admin.orders.destroy', ['order' => $order]) }}"
  method="POST">
  @csrf
  @method('DELETE')
  <button type="submit" class="btn btn-danger">Ištrinti</button>
</form>
@endslot
@endcomponent
@endsection

@section('content')
<div class="row">
  <div class="col">

    @component('components.admin.data-table')
    @slot('headings')
    <th scope="col">Paslauga</th>
    <th scope="col">Kaina</th>
    <th scope="col">Kiekis</th>
    <th scope="col">Suma &euro;</th>
    @endslot
    @foreach ($order->products as $product)
    <tr>
      <th scope="row">
        <a href={{ route('admin.orders.show', ['order' => $order]) }}>
          {{ $product->title }}
        </a>
      </th>
      <td>
        {{ $product->price }}
      </td>
      <td>
        {{ $product->pivot->quantity }}
      </td>
      <td>
        {{ $product->price * $product->pivot->quantity }}
      </td>
    </tr>
    @endforeach
    <tr>
      <th scope="row">
        Suma
      </th>
      <td></td>
      <td></td>
      <td>
        {{ $order->price }}
      </td>
    </tr>
    @endcomponent
  </div>
  <div class="col">
    <p>Vardas: {{ $order->customer_name }}</p>
    <p>El. paštas: {{ $order->customer_email }}</p>
    <p>Telefono nr.: {{ $order->customer_phone }}</p>
  </div>
</div>
@endsection