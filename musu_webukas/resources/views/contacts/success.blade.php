@extends('layouts.app')

@section('title', "Jūsų užklausa sėkmingai priimta")

@section('content')
<div class="container">
  <a role="button" class="btn btn-primary btn-lg btn-block" href="{{ route('products.index') }}">Grįžti į paslaugų
    sąrašą</a>
</div>
@endsection