@extends('layouts.app')

@section('title', 'Susisiekite su mumis')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container">
  <form action="{{ route('contacts.store') }}" method="POST">
    @csrf

    @component('components.admin.form-group', ['name' => 'name'])
    @slot('title') Vardas @endslot
    <input id="name" name="name" value="{{ old('name') }}" type="text"
      class="form-control @error('name') is-invalid @enderror" placeholder="Vardenis Pavardenis" required>
    @error('name')
    <div class="invalid-feedback">
      {{ $message }}
    </div>
    @enderror
    @endcomponent

    @component('components.admin.form-group', ['name' => 'phone'])
    @slot('title') Telefono numeris @endslot
    <input id="phone" name="phone" value="{{ old('phone') }}" type="text"
      class="form-control @error('phone') is-invalid @enderror" placeholder="+370..." required>
    @error('phone')
    <div class="invalid-feedback">
      {{ $message }}
    </div>
    @enderror
    @endcomponent

    @component('components.admin.form-group', ['name' => 'email'])
    @slot('title') El. pašto adresas @endslot
    <input id="email" name="email" value="{{ old('email') }}" type="email"
      class="form-control @error('email') is-invalid @enderror" placeholder="@" required>
    @error('email')
    <div class="invalid-feedback">
      {{ $message }}
    </div>
    @enderror
    @endcomponent

    @component('components.admin.form-group', ['name' => 'text'])
    @slot('title') Užklausa @endslot
    <textarea name="text" id="text" cols="30" rows="10" class="form-control @error('text') is-invalid @enderror"
      placeholder="Užklausa" required>{{ old('text') }}</textarea>
    @error('text')
    <div class="invalid-feedback">
      {{ $message }}
    </div>
    @enderror
    @endcomponent

    <hr class="mb-4">
    <button type="submit" class="btn btn-primary btn-lg btn-block">Klausti</button>
  </form>
</div>
@endsection