<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title')</title>

  <!-- Styles -->
  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Party Makers</a>
    <ul class="navbar-nav px-3">
      <li class="nav-item text-nowrap">
        <form action="{{ route('logout') }}" method="POST">
          @csrf
          <button type="submit" class="btn nav-link" href="#">Atsijungti</a>
        </form>
      </li>
    </ul>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            @component('components.admin.nav-item', [
            'icon' => 'file',
            'route' => 'admin.orders.index'])
            Užsakymai
            @endcomponent

            @component('components.admin.nav-item', [
            'icon' => 'shopping-cart',
            'route' => 'admin.products.index'])
            Paslaugos
            @endcomponent

            @component('components.admin.nav-item', [
            'icon' => 'list',
            'route' => 'admin.categories.index'])
            Kategorijos
            @endcomponent

            @component('components.admin.nav-item', [
            'icon' => 'mail',
            'route' => 'admin.contacts.index'])
            Užklausos
            @endcomponent
          </ul>
        </div>
      </nav>

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div
          class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 class="h2">@yield('title')</h1>
          @hasSection('buttons')
          <div class="btn-toolbar mb-2 mb-md-0">
            @yield('buttons')
          </div>
          @endif
        </div>

        @yield('content')
      </main>
    </div>
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/admin.js') }}" defer></script>
</body>

</html>