<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title')</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
  <header>
    <div class="collapse bg-dark" id="navbarHeader">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-7 py-4">
            <h4 class="text-white">Kodėl rinktis mus?</h4>
            <ul class="list-unstyled text-muted">
              <li>Per sukauptą darbo patirtį atsižvelgiame ne tik į Jūsų norus, bet ir gebame patarti, kad organizuojama
                šventė būtų išskirtinė bei ilgai neužmirštama.<</li> <li>Siūlome aukščiausios kokybės paslaugas už
                  prieinamą kainą.</li>
              <li>Darbą atliekame greitai ir kokybiškai.</li>
            </ul>
          </div>
          <div class="col-sm-4 offset-md-1 py-4">
            <ul class="list-unstyled">
              <li><a href="{{ route('products.index') }}" class="text-white">Paslaugos</a></li>
              <li><a href="{{ route('contacts.index') }}" class="text-white">Kontaktai</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="navbar navbar-dark bg-dark box-shadow">
      <div class="container d-flex justify-content-between">
        <a href="#" class="navbar-brand d-flex align-items-center">
          <strong>Party Makers</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
          aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </div>
  </header>

  <main role="main">
    <section class="jumbotron text-center">
      <div class="container">
        <h1>@yield('title')</h1>
      </div>
    </section>

    @yield('content')
  </main>


  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>