<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Get the products belonging to this category.
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
