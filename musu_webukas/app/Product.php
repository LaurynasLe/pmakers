<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'price', 'quantifiable', 'category_id'];

    /**
     * Get the category of this product.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * The orders that have this product.
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order')->using('App\OrderProduct')->withPivot('quantity');
    }
}
