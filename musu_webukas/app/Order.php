<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_name', 'customer_phone', 'customer_email'];

     /**
     * The products in this order.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->using('App\OrderProduct')->withPivot('quantity');
    }

    /**
     * Get the total price of the order.
     *
     * @return integer
     */
    public function getPriceAttribute()
    {
        return $this->products()->sum(DB::raw('products.price * order_product.quantity'));
    }
}
