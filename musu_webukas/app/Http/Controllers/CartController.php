<?php

namespace App\Http\Controllers;

use App\Order;
use App\Item;
use App\Product;
use App\Http\Requests\AddToCart;
use App\Http\Requests\Checkout;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Perform a checkout on the current cart
     *
     * @param  \App\Http\Requests\Checkout  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Checkout $request)
    {
        $cart = session('cart', []);
        $order = Order::create($request->validated());
        foreach ($cart as $product_id => $quantity) {
            $order->products()->attach($product_id, ['quantity' => $quantity]);
        }
        $request->session()->forget('cart');
        return view('cart.checkout', ['order' => $order]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = collect(session('cart', []));
        $items = $cart->map(function($quantity, $product_id) {
            $product = Product::find($product_id);
            return ['product' => $product, 'quantity' => $quantity, 'price' => $product->price * $quantity];
        });
        $price = $items->sum('price');
        return view('cart.index', ['items' => $items, 'price' => $price]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AddToCart  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddToCart $request)
    {
        $items = session('cart', []);
        $quantity = $request->input('quantity') ?? 1;
        $product_id = $request->input('product_id');
        if (array_key_exists($product_id, $items)) {
            $items[$product_id] += $quantity;
        } else {
            $items[$product_id] = $quantity;
        }
        session(['cart' => $items]);
        return redirect()->route('cart.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $product_id)
    {
        $items = collect(session('cart', []));
        unset($items[$product_id]);
        session(['cart' => $items]);
        return redirect()->route('cart.index');
    }
}
